﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Dapper;
using MySql.Data.MySqlClient;
using RecipeAPI.Models.AccountModels;
using System.Text;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace RecipeAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        IConfiguration _config;
        public AccountController(IConfiguration config)
        {
            _config = config;
        }
        [HttpPost("Register")]
        public JsonResult Register([FromBody]RegisterFormData data)
        {
            if (data == null || data.EmailAddress == null) return new JsonResult(new ErrorResult("Invalid email address"));
            if(data.EmailAddress == "") return new JsonResult(new ErrorResult("Invalid email address"));
            if(! data.EmailAddress.Contains("@")) return new JsonResult(new ErrorResult("Invalid email address"));
            try
            {
                var userguid = System.Guid.NewGuid().ToString();
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    db.Execute("insert into Users (Email, UserGuid) values (?email, ?guid)", new { email = data.EmailAddress, guid = userguid });
                }
                
                SendValidationEmail(data.EmailAddress, userguid);
            }
            catch (MySqlException ex)
            {
                if (ex.Message.Contains("Duplicate entry"))
                {
                    return new JsonResult(new ErrorResult("Email address already exists"));
                }
                else
                {
                    return new JsonResult(new ErrorResult("Error creating account"));
                }
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Error creating account"));
            }
            return new JsonResult(new SuccessResult());
        }

        private void SendEmail(string emailaddress, string subject, string body)
        {
            using (var smtp = new SmtpClient("smtpout.secureserver.net", 80))
            {
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("--emailaccount--", "--emailpassword--");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send("--emailaccount--", emailaddress, subject, body);
            }
        }

        private void SendValidationEmail(string emailaddress, string guid)
        {
            var strBody = new StringBuilder("Verify your email address at the following link:\n");
            //TODO Needs production address
            strBody.Append("http://localhost:8080/#/validateemail?email=" + emailaddress + "&guid=" + guid + "\n");

            SendEmail(emailaddress, "Validate Your Email Address", strBody.ToString());            
        }
        
        /*
        public string SendValidationEmailGmail(string emailaddress, string guid)
        {
            using (var smtp = new SmtpClient("smtp.gmail.com", 587))
            {
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("--emailaddress--", "--password--");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send("--emailaddress--", emailaddress, "Test Email from RecipeBook", "TestEmail from RecipeBook");
            }
            return "success?";
        }
        */
        /*
         * This function is called through a link provided by email.
         * They are provided with the user's GUID and must set an initial password
         * only works if the current password is set to NotSet and the user is not already verified
         */
        [HttpPost("VerifyEmailAddress")]
        public JsonResult VerifyEmailAddress([FromBody]VerifyEmailFormData data)
        {
            if(data == null || data.EmailAddress == null || data.UserGuid == null || data.Password == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }
            try
            {
                var password = GetPasswordHash(data.Password, data.UserGuid);
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var rowsaffected = db.Execute("update Users set Password = ?pwd, Verified = 1 where Email = ?emailaddress and UserGuid = ?guid and password = 'NotSet' and Verified = 0", 
                        new { pwd = password, emailaddress = data.EmailAddress, guid = data.UserGuid });

                    if (rowsaffected == 0)
                    {
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));                
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new SuccessResult());
        }

        private string GetPasswordHash(string password, string salt)
        {
            var passwordbytes = Encoding.GetEncoding("utf-8").GetBytes(password + salt);

            var sha256 = System.Security.Cryptography.SHA256.Create();
            var passwordhash = sha256.ComputeHash(passwordbytes);

            return Convert.ToBase64String(passwordhash);
        }

        /*
         * Creates an entry in a table to show that the account has requested an email reset
         * Sends a link with a guid created for this purpose to reset the password
         */
        [HttpPost("ForgotPassword")]
        public JsonResult ForgotPassword([FromBody] ForgotPasswordFormData data)
        {
            if (data == null || data.EmailAddress == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }
            try
            {
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var user = db.QueryFirstOrDefault<DbUsers>("select * from Users where Email = ?email",
                        new { email = data.EmailAddress }
                        );
                    if(user == null)
                    {
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }

                    var confirmationguid = Guid.NewGuid().ToString();
                    var expirationdate = DateTime.Now.AddMinutes(30);

                    var rowsaffected = db.Execute("insert into PasswordReset (Email, ConfirmationGuid, Expiration) values (?email, ?guid, ?expiration)",
                        new {email = user.Email, guid = confirmationguid, expiration = expirationdate }
                    );

                    SendResetPasswordLink(user.Email, confirmationguid);
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new SuccessResult());
        }

        private void SendResetPasswordLink(string emailaddress, string guid)
        {
            var strbody = new StringBuilder("Reset your password at the following link:\n");
            //TODO Needs production address
            strbody.Append("http://localhost:8080/#/resetpassword?email=" + emailaddress + "&guid=" + guid);

            SendEmail(emailaddress, "Password reset request", strbody.ToString());
        }

        /*
         * Resets the paassword of an account
         */
        [HttpPost("ResetPassword")]
        public JsonResult ResetPassword([FromBody] ResetPasswordFormData data)
        {
            if (data == null || data.EmailAddress == null || data.ConfirmationGuid == null || data.NewPassword == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }
            try
            {
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var resetconfirmation = db.QueryFirstOrDefault<DbPasswordReset>("select * from PasswordReset where Email = ?email and ConfirmationGuid = ?guid",
                        new {email = data.EmailAddress, guid = data.ConfirmationGuid}    
                    );
                    if (resetconfirmation == null)
                    {
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }
                    if(DateTime.Now >= resetconfirmation.Expiration)
                    {
                        db.Execute("delete from PasswordReset Where Id = ?id", new { id = resetconfirmation.Id });
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }

                    var user = db.QueryFirstOrDefault<DbUsers>("select * from Users where Email = ?email",
                        new { email = data.EmailAddress }
                    );
                    if(user == null)
                    {
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }
                    var password = GetPasswordHash(data.NewPassword,  user.UserGuid);

                    db.Execute("update Users set Password = ?pwd where Id = ?id",
                        new { pwd = password, id = user.Id }
                    );

                    db.Execute("delete from PasswordReset Where Id = ?id", new { id = resetconfirmation.Id });
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new SuccessResult());
        }

        [HttpPost("Login")]
        public JsonResult Login([FromBody]LoginFormData data)
        {
            if (data == null || data.EmailAddress == null || data.Password == null)
                return new JsonResult(new ErrorResult("Invalid request"));
            var email = data.EmailAddress;
            var password = data.Password;

            try
            {
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var user = db.QueryFirstOrDefault<DbUsers>("select * from Users where Email = ?email and Active =  1 and Verified = 1", new { email });
                    if (user != null)
                    {
                        var passwordbytes = Encoding.GetEncoding("utf-8").GetBytes(password + user.UserGuid);

                        var sha256 = System.Security.Cryptography.SHA256.Create();
                        var passwordhash = sha256.ComputeHash(passwordbytes);

                        var pwd = Convert.ToBase64String(passwordhash);
                        if(pwd == user.Password)
                        {
                            var jwttoken = CreateBearerToken(user);
                            return new JsonResult(new SuccessResult(
                                new {
                                    emailaddress = user.Email,
                                    username = user.Username ?? "",
                                    token = jwttoken
                                }
                            ));
                        }
                        else
                        {
                            return new JsonResult(new ErrorResult("Login attempt failed"));
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new ErrorResult("Login attempt failed"));
        }

        private string CreateBearerToken(DbUsers user)
        {
            var username = user.Username;
            if (username == null)
                username = "";
            var claims = new[]
            {
                new Claim("id", user.Id.ToString()),
                new Claim("emailaddress", user.Email),
                new Claim("username", username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["JWT:Issuer"],
                _config["JWT:Audience"], claims, expires: DateTime.Now.AddDays(30), signingCredentials: creds);


            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [HttpGet, Authorize]
        public JsonResult GetAccount()
        {
            var uid = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "id").Value;
            
            if (int.TryParse(uid, out int userid))
            {
                try
                {
                    using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                    {
                        var u = db.QueryFirstOrDefault<UserResult>("select * from Users where id = ?userid", new { userid });
                        if (u != null)
                        {
                            return new JsonResult(new SuccessResult(u));
                        }
                    }
                }
                catch (MySqlException ex)
                {
                    return new JsonResult(new ErrorResult("Unknown database error"));
                }
                catch (Exception ex)
                {
                    return new JsonResult(new ErrorResult("Unknown error"));
                }                
            }
            return new JsonResult(new ErrorResult("User could not be found"));
        }

        [HttpPost, Authorize]
        public JsonResult SaveAccount([FromBody] SaveAccountFormData data)
        {
            if(data == null || data.Username == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }

            var uid = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "id").Value;
            if(uid == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }
            try
            {
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var rowsaffected = db.Execute("update Users set Username = ?username where Id = ?userid", new { username = data.Username, userid = uid });
                    if (rowsaffected > 0)
                    {
                        return new JsonResult(new SuccessResult("User successfully saved"));
                    }
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new ErrorResult("Invalid request"));
        }

        [HttpPost("ChangePassword"), Authorize]
        public JsonResult ChangePassword([FromBody] ChangePasswordFormData data)
        {
            if (data == null || data.NewPassword == null  || data.ExistingPassword == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }

            var uid = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "id").Value;
            if (uid == null)
            {
                return new JsonResult(new ErrorResult("Invalid request"));
            }
            try
            {
                using (var db = new MySqlConnection(_config["MySqlConnectionString"]))
                {
                    var user = db.QueryFirstOrDefault<DbUsers>("select * from Users where Id = ?id", new { id = uid });
                    if(user == null) return new JsonResult(new ErrorResult("Invalid request"));

                    var existingPasswordHash = user.Password;
                    var userGuid = user.UserGuid;
                    if(existingPasswordHash != GetPasswordHash(data.ExistingPassword, userGuid))
                    {
                        return new JsonResult(new ErrorResult("Invalid request"));
                    }

                    var newPasswordHash = GetPasswordHash(data.NewPassword, userGuid);

                    var rowsaffected = db.Execute("update Users set Password = ?password where Id = ?userid", new { password = newPasswordHash, userid = uid });
                    if (rowsaffected > 0)
                    {
                        return new JsonResult(new SuccessResult("User successfully saved"));
                    }
                }
            }
            catch (MySqlException ex)
            {
                return new JsonResult(new ErrorResult("Unknown database error"));
            }
            catch (Exception ex)
            {
                return new JsonResult(new ErrorResult("Unknown error"));
            }
            return new JsonResult(new ErrorResult("Invalid request"));
        }
    }
}