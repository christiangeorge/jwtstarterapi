﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeAPI.Models.AccountModels
{
    public class ErrorResult 
    {
        public string Status { get; set; }
        public string ErrorMessage { get; set; }

        public ErrorResult(string message)
        {
            Status = "error";
            ErrorMessage = message;
        }
    }

    public class SuccessResult
    {
        public string Status { get; set; }
        public object Data { get; set; }

        public SuccessResult(object data = null)
        {
            Status = "success";
            if (data != null)
            {
                Data = data;
            }
        }
    }
    
    public class UserResult
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public bool Active { get; set; }
        public bool Verified { get; set; }
    }


}
