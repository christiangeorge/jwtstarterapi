﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeAPI.Models.AccountModels
{
    public class RegisterFormData
    {
        public string EmailAddress { get; set; }
    }

    public class ForgotPasswordFormData
    {
        public string EmailAddress { get; set; }
    }

    public class VerifyEmailFormData
    {
        public string EmailAddress { get; set; }
        public string UserGuid { get; set; }
        public string Password { get; set; }
    }

    public class ResetPasswordFormData
    {
        public string EmailAddress { get; set; }
        public string ConfirmationGuid { get; set; }
        public string NewPassword { get; set; }
    }
    
    public class LoginFormData
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }

    public class SaveAccountFormData
    {
        public string Username { get; set; }
    }

    public class ChangePasswordFormData
    {
        public string NewPassword { get; set; }
        public string ExistingPassword { get; set; }
    }
}
