﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeAPI.Models.AccountModels
{
    public class DbUsers
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public bool Verified { get; set; }
        public string UserGuid { get; set; }
    }

    public class DbPasswordReset
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string ConfirmationGuid { get; set; }
        public DateTime Expiration { get; set; }
    }
}
